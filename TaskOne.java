import java.util.Scanner;

public class TaskOne {
    public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter number: ");
		int number = input.nextInt();
		System.out.println("You entered the number: " + number);
	}
}